# https://doc.qt.io/QtForDeviceCreation/qtee-meta-qt5.html
do_configure_prepend() {
    cat > ${S}/mkspecs/oe-device-extra.pri <<EOF
    QMAKE_LIBS_EGL += -lEGL -ldl -lglib-2.0 -lpthread
    QMAKE_LIBS_OPENGL_ES2 += -lGLESv2 -lgsl -lEGL -ldl -lglib-2.0 -lpthread

    QMAKE_CFLAGS += -DLINUX=1 -DWL_EGL_PLATFORM
    QMAKE_CXXFLAGS += -DLINUX=1 -DWL_EGL_PLATFORM

    QT_QPA_DEFAULT_PLATFORM = eglfs
EOF
}

PACKAGECONFIG_append = " accessibility eglfs fontconfig gles2 linuxfb tslib"
PACKAGECONFIG_remove = "tests x11"

DEPENDS += "userland"
