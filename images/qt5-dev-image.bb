SUMMARY = "A basic Qt5 qwidgets dev image"

require qt5-basic-image.bb

QT_DEV_TOOLS = " \
    qtbase-dev \
    qtbase-mkspecs \
    qtbase-plugins \
    qtbase-tools \
    qtserialport-dev \
    qtserialport-mkspecs \
"

IMAGE_INSTALL += " \
    ${QT_DEV_TOOLS} \
"

export IMAGE_BASENAME = "qt5-dev-image"