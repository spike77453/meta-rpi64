SUMMARY = "A basic Qt5 qwidgets image"

IMAGE_FEATURES += "package-management"
IMAGE_LINGUAS = "en-us"

inherit image
inherit populate_sdk populate_sdk_qt5

CORE_OS = " \
    openssh openssh-keygen openssh-sftp-server \
    packagegroup-core-boot \
    tzdata \
    systemd-bootchart \
    rsync \
"

KERNEL_EXTRA = " \
    kernel-modules \
"

QT_TOOLS = " \
    qtbase \
    qtserialport \
"

FONTS = " \
    fontconfig \
    fontconfig-utils \
    ttf-bitstream-vera \
"

TSLIB = " \
    tslib \
    tslib-conf \
    tslib-calibrate \
    tslib-tests \
"

LIBS = " \
    boost \
    fmt \
"

IMAGE_INSTALL += " \
    ${CORE_OS} \
    ${KERNEL_EXTRA} \ 
    ${QT_TOOLS} \
    ${FONTS} \
    ${TSLIB} \
    ${LIBS} \
"
export IMAGE_BASENAME = "qt5-basic-image"
