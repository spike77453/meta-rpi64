# meta-rpi64

 Yocto meta-layer for 64-bit RPi builds

 Layer depends on:

 ```
URI: git://git.yoctoproject.org/poky.git
branch: dunfell

URI: git://git.yoctoproject.org/meta-raspberrypi
branch: dunfell

URI: git://git.openembedded.org/meta-openembedded
branch: dunfell

URI: https://github.com/meta-qt5/meta-qt5
branch: dunfell
```

Clone meta layers:

```
export TOPDIR=${HOME}/poky-dunfell
git clone -b dunfell git://git.yoctoproject.org/poky.git ${TOPDIR}
pushd ${TOPDIR}
git clone -b dunfell git://git.yoctoproject.org/meta-raspberrypi
git clone -b dunfell git://git.openembedded.org/meta-openembedded
git clone -b dunfell https://github.com/meta-qt5/meta-qt5
git clone -b dunfell https://gitlab.com/spike77453/meta-rpi64
popd
```

A sample configuration is available at https://gitlab.com/spike77453/rpi64-build  
To start baking, run

```
git clone https://gitlab.com/spike77453/rpi64-build
source ${TOPDIR}/oe-init-build-env rpi64-build
bitbake qt5-basic-image
bitbake qt5-basic-image -c populate_sdk
```

Check the local configuration at `rpi64-build/conf/local.conf`. The default `DL_DIR`, `SSTATE_DIR` and `TMPDIR` point to `/data/` so make sure, there's enough (at least ~150GB) space available.